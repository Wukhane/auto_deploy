from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel
import subprocess
import tempfile
import os
import json

app = FastAPI()

# Function to add Helm repository
def add_helm_repo(repo_name, repo_url):
    try:
        subprocess.run(["helm", "repo", "add", repo_name, repo_url], check=True)
        subprocess.run(["helm", "repo", "update"], check=True)
    except subprocess.CalledProcessError as e:
        raise RuntimeError(f"Failed to add Helm repository {repo_name} with URL {repo_url}: {e}")

# Pydantic model for input data
class NamespaceData(BaseModel):
    namespace: str
    domain: str

# Fixed values to be used in deployment
fixed_values = {
    "applicationName": "my-first-app",
    "image": {
        "frontend": "dridibilel/gitlab_ci:frontend",
        "backend": "dridibilel/gitlab_ci:backend"
    },
    "service": {
        "frontendPort": 3000,
        "backendPort": 5000,
        "type": "NodePort"
    },
    "ingress": {
        "enabled": True,
        "paths": [
            {"path": "/", "backendService": "frontend", "port": 3000},
            {"path": "/api", "backendService": "backend", "port": 5000}
        ]
    }
}

# Endpoint to deploy the app
@app.post("/deploy")
async def deploy_app(data: NamespaceData):
    try:
        # Add Helm repository dynamically
        add_helm_repo("helm-repo", "https://bepunpun.github.io/helm/")

        # Create deployment data by merging fixed values with the provided namespace and domain
        deployment_data = {
            **fixed_values,
            "namespace": data.namespace,
            "ingress": {**fixed_values["ingress"], "domain": data.domain}
        }

        # Convert the deployment data to a temporary JSON file
        with tempfile.NamedTemporaryFile(mode='w', delete=False) as temp_file:
            temp_file.write(json.dumps(deployment_data))
            temp_file_path = temp_file.name

        # Run Helm install/upgrade command
        subprocess.run(
            ["helm", "upgrade", "--install", "my-first-app-release", "helm-repo/mern-app", 
             "--namespace", data.namespace, "--create-namespace", "-f", temp_file_path], 
            check=True
        )

        # Clean up temporary file
        os.unlink(temp_file_path)

        return {"message": "App deployment initiated successfully!"}
    except subprocess.CalledProcessError as e:
        return JSONResponse(status_code=400, content={"error": f"Helm command failed with error code {e.returncode}: {e.stderr.decode('utf-8')}"})
    except Exception as e:
        return JSONResponse(status_code=500, content={"error": str(e)})

# Endpoint to uninstall the app
@app.post("/undeploy")
async def undeploy_app(data: NamespaceData):
    try:
        # Run Helm uninstall command
        subprocess.run(["helm", "uninstall", "my-first-app-release", "--namespace", data.namespace], check=True)
        return {"message": "App uninstalled successfully!"}
    except subprocess.CalledProcessError as e:
        return JSONResponse(status_code=400, content={"error": f"Helm command failed with error code {e.returncode}: {e.stderr.decode('utf-8')}"})
    except Exception as e:
        return JSONResponse(status_code=500, content={"error": str(e)})

# Main entry point
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)