FROM python:3.9-slim

WORKDIR /app

COPY ./app /app

RUN pip install --no-cache-dir fastapi uvicorn

# Install Git
RUN apt-get update && apt-get install -y git

# Install helm
RUN apt-get update && apt-get install -y curl && \
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod +x get_helm.sh && \
    ./get_helm.sh


# Expose the necessary ports for the application
EXPOSE 8000

# Start the application
CMD ["uvicorn", "deploy_service:app", "--host", "0.0.0.0", "--port", "8000"]